<?php

return [
    'voltar' => "Go back",
    'sucesso' => "Login successfully.",
    'senhaalterada' => "Password changed successfully.",
    'alterada' => "Password changed successfully. You can now login.",
    'perdeusenha?' => "Lost your password?",
    'semproblema' => "No problems! Enter your email address to verify your account.",
    'insiraemail' => "Enter your email",
    'recuperarsenha' => "Recover Password",
    'verifique' => "Please verify your email with the activation code.",
    'codigoativacao' => "Activation code",
    'insiracodigoativacao' => "Enter activation code",
    'novasenha' => "New password",
    'insiranovasenha' => "Enter the new password",
    'alterarsenha' => "Change Password",
    'contacriadasucesso' => "Account successfully created. Account activation via email is still required.",
    'atualizandodados' => "Atualizando dados do usuário.",
    'alteraremail' => "Change e-mail",


    'dados' => "Data",
    'atualizardadosusuario' => "Updating user data.",
    'alterarsenha' => "Change Password",
    'desativarconta' => "Deactivate account",
    'cadastro' => "Register",
    'nome' => "Name",
    'digiteseunome' => "Enter your name",
    'camponomeobrigatorio' => "The name field is required.",
    'camponomeletras' => "The name field must be longer than 2 letters.",
    'sobrenome' => "Surname",
    'digiteseusobrenome' => "Enter your surname",
    'camposobrenomeobrigatorio' => "The surname is required.",
    'camposobrenomeletras' => "The surname field must be longer than 2 letters.",
    'atualizar' => "Update",

    'paginalogin' => "login",
    'paginaerrada' => "Wrong page",
    'realizarlogin' => "Login required",

    'activation_email_sent' => "Account successfully created. Account activation via email is still required.",
    'informacoesinvalidas' => "Invalid information.",
];
