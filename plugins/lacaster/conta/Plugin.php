<?php namespace Lacaster\Conta;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * conta Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.conta::lang.plugin.name',
            'description' => 'lacaster.conta::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\Conta\Components\ContaEsqueceuSenha' => 'contaEsqueceuSenha',
            'Lacaster\Conta\Components\ContaLogin' => 'contaLogin',
            'Lacaster\Conta\Components\ContaRegistrar' => 'contaRegistrar',
            'Lacaster\Conta\Components\ContaDados' => 'contaDados',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.conta.some_permission' => [
                'tab' => 'lacaster.conta::lang.plugin.name',
                'label' => 'lacaster.conta::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'conta' => [
                'label'       => 'lacaster.conta::lang.plugin.name',
                'url'         => Backend::url('lacaster/conta/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.conta.*'],
                'order'       => 500,
            ],
        ];
    }
}
