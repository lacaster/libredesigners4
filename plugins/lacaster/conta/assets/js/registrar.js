$(function(){

    var octopus = {
        init: function() {
            view.init();
        }
    };

    // Renomear
    var view = {
        init: function() {
            this.btSubmit = $(".profile #bt-cadastrar");
            this.profile = {
                email: $(".profile .email"),
                name: $(".profile .nome"),
                surName: $(".profile .sobrenome"),
                password: $(".profile .senha"),
                confirmPassword: $(".profile .confirmar_senha"),
            };
            this.form = $("form.profile");
            view.events();
        },

        events: function() {
            this.btSubmit.click(this.onClickSubmitButton.bind(this));
            this.associaMascara();

        },

        onClickSubmitButton: function(e) {
            e.preventDefault();
            var isCamposValidos = false;
            isCamposValidos = view.validaCampos();
            if(isCamposValidos){
                $("form.profile").request("onRegister");
            }

        },

        validaCampos: function() {

            var $flag = true;

            var $email = this.profile.email.find("input");
            var $emailHelpBlock = this.profile.email.find(".help-block");

            var $name = this.profile.name.find("input");
            var $nameHelpBlock = this.profile.name.find(".help-block");

            var $surName = this.profile.surName.find("input");
            var $surNameHelpBlock = this.profile.surName.find(".help-block");

            var $password = this.profile.password.find("input");
            var $passwordHelpBlock = this.profile.password.find(".help-block");

            var $confirmPassword = this.profile.confirmPassword.find("input");
            var $confirmPasswordHelpBlock = this.profile.confirmPassword.find(".help-block");

            // Limpa todas as classes "erro"
            $(".help-block").removeClass("erro");
            $(".form-group input").removeClass("erro");



            // Verifica cada campo individualmente

            // email
            if ($email.val() === "") {
                $email.addClass("erro");
                $emailHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }
            //validar formato de e-mail

            var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!emailRegExp.exec($email.val())) {
                $email.addClass("erro");
                $emailHelpBlock.filter(".formato").addClass("erro");
                $flag = false;
            }
            //
            //
            // nome
            if ($name.val() === "") {
                $name.addClass("erro");
                $nameHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if ($name.val().length < 2) {
                $name.addClass("erro");
                $nameHelpBlock.filter(".minimo").addClass("erro");
                $flag = false;
            }


            // sobreNome
            if ($surName.val() === "") {
                $surName.addClass("erro");
                $surNameHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if ($surName.val().length < 2) {
                $surName.addClass("erro");
                $surNameHelpBlock.filter(".minimo").addClass("erro");
                $flag = false;
            }


            // senha
            if ($password.val() === "") {
                $password.addClass("erro");
                $passwordHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if($password.val().length < 9) {

                $password.addClass("erro");
                $passwordHelpBlock.filter(".minimo").addClass("erro");
                $flag = false;

            }

            // confirma senha
            if ($confirmPassword.val() === "") {
                $confirmPassword.addClass("erro");
                $confirmPasswordHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if($password.val() !== $confirmPassword.val()){
                $confirmPassword.addClass("erro");
                $confirmPasswordHelpBlock.filter(".igual").addClass("erro");
                $flag = false;
            }





            return $flag;
        },


        associaMascara: function() {

            $(".profile .nome input").mask('Z',{translation: {'Z': {pattern: /[a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ]/, recursive: true}}});

            $(".profile .sobrenome input").mask('Z',{translation: {'Z': {pattern: /[a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ]/, recursive: true}}});

            $(".profile .email input").mask('Z',{translation: {'Z': {pattern: /[\w@\-.+]/, recursive: true}}});

        },

        render: function(){
        }
    };

    octopus.init();
});
