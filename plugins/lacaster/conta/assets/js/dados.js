$(function() {

    var model = {
        init: function() {}
    };

    var octopus = {
        init: function() {
            model.init();
            view.init();

        }
    };

    var view = {

        init: function() {

            this.bt_atualizar = $("#bt-atualizar");

            this.cadastro = {
                nome: $(".profile .nome"),
                sobrenome: $(".profile .sobrenome"),
                email: $(".profile .email"),
            };

            view.events();

        },

        events: function() {

            this.associaMascara();
            this.bt_atualizar.click(this.onClickSubmitButton.bind(this));

        },

        onClickSubmitButton: function(e){

            e.preventDefault();
            var isCamposValidos = false;
            isCamposValidos = view.validaCampos();
            if(isCamposValidos){
                $("form.profile").request("onUpdateUser");
            }


        },


        associaMascara: function() {
            $(".profile .nome input").mask('Z',{translation: {'Z': {pattern: /[a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ]/, recursive: true}}});
            $(".profile .sobrenome input").mask('Z',{translation: {'Z': {pattern: /[a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ]/, recursive: true}}});
            $(".profile .email input").mask('Z',{translation: {'Z': {pattern: /[\w@\-.+]/, recursive: true}}});
        },

        validaCampos: function() {

            var $flag = true;

            // Captura inputs e help blocks
            var $nome = this.cadastro.nome.find("input");
            var $nomeHelpBlock = this.cadastro.nome.find(".help-block");

            var $sobrenome = this.cadastro.sobrenome.find("input");
            var $sobrenomeHelpBlock = this.cadastro.sobrenome.find(".help-block");

            var $email = this.cadastro.email.find("input");
            var $emailHelpBlock = this.cadastro.email.find(".help-block");

            // Limpa todas as classes "erro"
            $(".help-block").removeClass("erro");
            $(".campo input").removeClass("erro");

            // Verifica cada campo individualmente
            // nome
            if ($nome.val() === "") {
                $nome.addClass("erro");
                $nomeHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if ($nome.val().length < 2) {
                $nome.addClass("erro");
                $nomeHelpBlock.filter(".minimo").addClass("erro");
                $flag = false;
            }

            // sobreNome
            if ($sobrenome.val() === "") {
                $sobrenome.addClass("erro");
                $sobrenomeHelpBlock.filter(".obrigatorio").addClass("erro");
                $flag = false;
            }

            if ($sobrenome.val().length < 2) {
                $sobrenome.addClass("erro");
                $sobrenomeHelpBlock.filter(".minimo").addClass("erro");
                $flag = false;
            }




            return $flag;

        },


    };

    octopus.init();

});

