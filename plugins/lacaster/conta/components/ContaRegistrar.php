<?php namespace Lacaster\Conta\Components;


use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Session;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Winter\User\Models\Settings as UserSettings;
use Lacaster\Profile\Models\Profile as Profile;
use Exception;
use Winter\User\Components\Account as UserAccount;
use System\Classes\CombineAssets;
use Log;

class ContaRegistrar extends UserAccount
{
    public function componentDetails()
    {
        return [
            'name'        => 'Registra novo cliente',
            'description' => 'No description provided yet...'
        ];
    }


    public function onRun() {
        $assetsCss = [
            'assets/css/registrar.css',
        ];

        $assetsJS = [
            'assets/js/libs/jquery.mask.min.js',
            'assets/js/helpers.js',
            'assets/js/registrar.js'
        ];

        $this->addJs(CombineAssets::combine($assetsJS, base_path('plugins/lacaster/conta')));
        $this->addCss(CombineAssets::combine($assetsCss, base_path('plugins/lacaster/conta')));

        /*
         * Redirect to HTTPS checker
         */
        if ($redirect = $this->redirectForceSecure()) {
            return $redirect;
        }

        /*
         * Activation code supplied
         */
        if ($code = $this->activationCode()) {
            $this->onActivate($code);
	    return Redirect::to("/");
        }

        $this->prepareVars();

    }



    public function onRegister()
    {
        try {
            if (!$this->canRegister()) {
                throw new ApplicationException(Lang::get(/*Registrations are currently disabled.*/'.user::lang.account.registration_disabled'));
            }


            /*
             * Validate input
             */
            $data = post();

            $rules = [
                'email'    => 'required|email|between:6,255|unique:users,email',
                'password' => 'required|between:4,255|confirmed',
                'name' => 'required|alpha',
                'surname' => 'required|alpha',


            ];


/*            $messages = [*/
                //'required' => "O campo :attribute é obrigatório.",
                //'email' => "O campo e-mail está mal formatado.",
                //'alpha' => "O campo :attribute deve conter apenas letras.",
                //'between' => 'O campo :attribute deve conter entre :min - :max caracteres.',
                //'numeric' => "O campo :attribute deve conter apenas números.",
                //'confirmed' => "A confirmação da :attribute não condiz com a :attribute.",
                //'digits' => "O campo :attribute deve conter :digits dígitos.",
                //'unique' => "Este :attribute já foi escolhido anteriormente."
            /*];*/


            //$validation = Validator::make($data, $rules, $messages);
            $validation = Validator::make($data, $rules);

            $attributeNames = array(
                'email' => 'E-mail',
                'password' => 'Senha',
                'name' => 'Nome',
                'surname' => 'Sobrenome',
            );

            $validation->setAttributeNames($attributeNames);

            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            /*
             * Register user
             */

            Event::fire('winter.user.beforeRegister', [&$data]);

            $requireActivation = UserSettings::get('require_activation', true);
            $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
            $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;

            $user = Auth::register($data, $automaticActivation);

            /*
             * Generate profile
             */
            Event::fire('winter.user.register', [$user, $data]);

            /*
             * Activation is by the user, send the email
             */

            if ($userActivation) {
                $this->sendActivationEmail($user);

                //Flash::success(Lang::get([>An activation email has been sent to your email address.<]'.user::lang.account.activation_email_sent'));
                Lang::get(/*An activation email has been sent to your email address.*/'lacaster.conta::lang.activation_email_sent');

            }

            /*
             * Automatically activated or not required, log the user in
             */
            if ($automaticActivation || !$requireActivation) {
                Auth::login($user);
            }

            /*
             * Redirect to the intended page after successful sign in
             */

            Flash::success(Lang::get('lacaster.conta::lang.activation_email_sent'));

            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
        }
        catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }
    }

    /**
     * Trigger a subsequent activation email
     */
    public function onSendActivationEmail()
    {
        try {
            if (!$user = $this->user()) {
                throw new ApplicationException(Lang::get(/*You must be logged in first!*/'.user::lang.account.login_first'));
            }

            if ($user->is_activated) {
                throw new ApplicationException(Lang::get(/*Your account is already activated!*/'.user::lang.account.already_active'));
            }

            Flash::success(Lang::get(/*An activation email has been sent to your email address.*/'lacaster.conta::lang.conta_ativacao_enviada'));

            $this->sendActivationEmail($user);

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }


    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);

        $link = $this->makeActivationUrl($code);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];



        $language = \Winter\Translate\Classes\Translator::instance()->getLocale();

	Mail::send('winter.user::mail.activate', $data, function($message) use ($user) {
		$message->to($user->email, $user->name);
	});


    }


    /**
     * Activate the user
     * @param  string $code Activation code
     */
    public function onActivate($code = null)
    {
        try {
            $code = post('code', $code);

            $errorFields = ['code' => 'Código de ativação inválido.'];

            /*
             * Break up the code parts
             */
            $parts = explode('!', $code);
            if (count($parts) != 2) {
                throw new ValidationException($errorFields);
            }

            list($userId, $code) = $parts;

            if (!strlen(trim($userId)) || !strlen(trim($code))) {
                throw new ValidationException($errorFields);
            }

            if (!$user = Auth::findUserById($userId)) {
                throw new ValidationException($errorFields);
            }

            if (!$user->attemptActivation($code)) {
                throw new ValidationException($errorFields);
            }

            Flash::success("Conta ativada com sucesso.");

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }
}
