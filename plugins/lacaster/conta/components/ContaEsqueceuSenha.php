<?php namespace Lacaster\Conta\Components;

use Winter\User\Components\ResetPassword;
use Auth;
use Lang;
use Mail;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use Winter\User\Models\User as UserModel;
use System\Classes\CombineAssets;

class ContaEsqueceuSenha extends ResetPassword
{
    public function componentDetails()
    {
        return [
            'name'        => 'ContaEsqueceuSenha Component',
            'description' => 'No description provided yet...'
        ];
    }


    public function onRun() {
        $assetsCss = [
            'assets/css/login.css',
        ];

        $assetsJS = [
            'assets/js/libs/jquery.mask.min.js',
        ];

        $this->addJs(CombineAssets::combine($assetsJS, base_path('plugins/lacaster/conta')));
        $this->addCss(CombineAssets::combine($assetsCss, base_path('plugins/lacaster/conta')));

    }



    public function onRestorePassword()
    {
        $rules = [
            'email' => 'required|email|between:6,255'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user = UserModel::findByEmail(post('email'));
        if (!$user || $user->is_guest) {
            throw new ApplicationException(Lang::get(/*A user was not found with the given credentials.*/'winter.user::lang.account.invalid_user'));
        }

        $code = implode('!', [$user->id, $user->getResetPasswordCode()]);

        $link = $this->makeResetUrl($code);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        $language = \Winter\Translate\Classes\Translator::instance()->getLocale();

	Mail::send('winter.user::mail.restore', $data, function($message) use ($user) {
		$message->to($user->email, $user->full_name);
	    });


    }
}
