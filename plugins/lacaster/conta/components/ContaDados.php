<?php namespace Lacaster\Conta\Components;

use Cms\Classes\ComponentBase;
use System\Classes\CombineAssets;
use Winter\User\Components\Account as UserAccount;
use Auth;
use Flash;

class ContaDados extends UserAccount
{

    public $usuario;
    public $is_auth;

    public function componentDetails()
    {
        return [
            'name'        => 'ContaDados Component',
            'description' => 'Formulário de edição dos dados'
        ];
    }

    public function onRun() {
        $assetsCss = [
            'assets/css/usuario.css',
        ];

        $assetsJS = [
            'assets/js/libs/jquery.mask.min.js',
            'assets/js/dados.js',
        ];

        $this->addJs(CombineAssets::combine($assetsJS, base_path('plugins/lacaster/conta')));
        $this->addCss(CombineAssets::combine($assetsCss, base_path('plugins/lacaster/conta')));

        $this->is_auth = Auth::check();

        if($this->is_auth){
            $this->usuario = Auth::getUser();
        } else {
            $this->usuario = null;
        }

    }

    public function onUpdateUser()
    {
        $data = post();
        $usuario = Auth::getUser();

        $usuario->name = $data["nome"];
        $usuario->surname = $data["sobrenome"];
        $usuario->save();

        Flash::success("Dados atualizados com sucesso");
    }

    public function defineProperties()
    {
        return [];
    }
}
