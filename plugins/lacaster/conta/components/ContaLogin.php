<?php namespace Lacaster\Conta\Components;

use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Winter\User\Models\Settings as UserSettings;
use Lacaster\Profile\Models\Profile as Profile;
use Exception;
use Winter\User\Components\Account as UserAccount;
use System\Classes\CombineAssets;

class ContaLogin extends UserAccount{

    public function componentDetails()
    {
        return [
            'name'        => 'Login Form Component',
            'description' => 'Formulário de Login'
        ];
    }

    public function onRun() {
        $assetsCss = [
            'assets/css/login.css',
        ];

        $this->addCss(CombineAssets::combine($assetsCss, base_path('plugins/lacaster/conta')));

    }



    public function onSignin()
    {
        /*
            * Validate input
            */
        $data = post();
        $rules = [];

        $rules['login'] = $this->loginAttribute() == UserSettings::LOGIN_USERNAME
            ? 'required|between:2,255'
            : 'required|email|between:6,255';

        $rules['password'] = 'required|between:4,255';

        if (!array_key_exists('login', $data)) {
            $data['login'] = post('username', post('email'));
        }

        $validation = Validator::make($data, $rules);

        $attributeNames = array(
            'login' => 'E-mail',
            'password' => 'Senha',
        );

        $validation->setAttributeNames($attributeNames);


        if ($validation->fails()) {
            //Flash::error(Lang::get('lacaster.conta::lang.informacoesinvalidas'));
            throw new ValidationException($validation);
        }

        /*
            * Authenticate user
            */
        $credentials = [
            'login'    => array_get($data, 'login'),
            'password' => array_get($data, 'password')
        ];


        Event::fire('winter.user.beforeAuthenticate', [$this, $credentials]);





        $user = Auth::authenticate($credentials, true);

        if ($user->isBanned()) {
            Auth::logout();
            throw new AuthException(/*Sorry, this user is currently not activated. Please contact us for further assistance.*/'winter.user::lang.account.banned');
        }

        /*
            * Redirect
            */

        Flash::success(Lang::get('lacaster.conta::lang.sucesso'));
        if ($redirect = $this->makeRedirection(true)) {
            return $redirect;
        }
    }
}
