<?php namespace Lacaster\SuperDestaque;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * superDestaque Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.superdestaque::lang.plugin.name',
            'description' => 'lacaster.superdestaque::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return []; // Remove this line to activate

        return [
            'Lacaster\SuperDestaque\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.superdestaque.some_permission' => [
                'tab' => 'lacaster.superdestaque::lang.plugin.name',
                'label' => 'lacaster.superdestaque::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'superdestaque' => [
                'label'       => 'lacaster.superdestaque::lang.plugin.name',
                'url'         => Backend::url('lacaster/superdestaque/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.superdestaque.*'],
                'order'       => 500,
            ],
        ];
    }
}
