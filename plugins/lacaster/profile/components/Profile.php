<?php namespace Lacaster\Profile\Components;

use Cms\Classes\ComponentBase;
use Winter\Blog\Models\Post;
use Backend\Models\User;
use Lacaster\LikeBlogPost\Models\LikeButton;

class Profile extends ComponentBase
{
    public $profile;
    private $user_id;
    /**
     * Gets the details for the component
     */
    public function componentDetails()
    {
        return [
            'name'        => 'profile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'user_id' => [
                'label'             => "User id",
                'description'       => "Id do usuário",
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'default'           => '{{ :id }}'
            ],
        ];
    }

    public function onRun(){
        $this->prepareVars();
        $this->addCss('/plugins/lacaster/profile/assets/css/profile.css');
        $this->profile = $this->loadProfile();
    }

    protected function loadProfile() {
        $user = User::findOrFail($this->user_id);
        return $user->profile;
    }

    protected function prepareVars(){
        $this->user_id = $this->page['user_id'] = $this->param('id');
    }
}
