<?php namespace Lacaster\Profile\Components;

use Cms\Classes\ComponentBase;
use Backend\Models\User;
use Winter\Blog\Models\Post;

class ProfilePost extends ComponentBase
{


    public $profile;
    private $blogPosts;
    private $user_id;
    /**
     * Gets the details for the component
     */
    public function componentDetails()
    {
        return [
            'name'        => 'ProfilePost Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'rainlab.blog::lang.settings.post_slug',
                'description' => 'rainlab.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    public function onRun(){
        $this->prepareVars();
        $this->addCss('/plugins/lacaster/profile/assets/css/profile.css');
        $this->profile = $this->loadProfile();
    }

    protected function loadProfile() {
        $user = User::findOrFail($this->user_id);
        return $user->profile;
    }

    protected function prepareVars(){
        $slug = $this->property('slug');

        $post = new Post;

        $post = $post->isClassExtendedWith('Winter.Translate.Behaviors.TranslatableModel')
            ? $post->transWhere('slug', $slug)
            : $post->where('slug', $slug);


        $post = $post->first();


        $this->user_id = $this->page['user_id'] = $post->user_id;
    }
}
