<?php namespace Lacaster\Profile\Components;

use Cms\Classes\ComponentBase;
use Winter\Blog\Components\Posts;
use Winter\Blog\Models\Post as BlogPost;
use Lacaster\LikeBlogPost\Models\LikeButton;
use Lacaster\CatClub\Models\Comments;
use Backend\Models\User;


class PostsProfile extends Posts
{

    public $user_id;
    public $posts;
    public $profile;
    public $count_posts;
    public $count_likes;
    public $count_comments;

    public function componentDetails()
    {
        return [
            'name'        => 'PostsProfile Component',
            'description' => 'Display posts of a user...'
        ];
    }

    public function setValuesExtended()
    {
        $posts = $this->posts;
        $count_likes = [];
        $count_comments = [];

        foreach ($posts as $p) {
            $count_likes[$p->id] = LikeButton::where("blogpost_id",$p->id)->count();
            $count_comments[$p->id] = Comments::where("post_id",$p->id)->count();
        }

        $this->count_comments = $count_comments;
        $this->count_likes = $count_likes;
    }


    public function onRun(){
        $this->prepareVars();

        $this->user_id = $this->page['user_id'] = $this->param('id');
        $this->category = $this->page['category'] = $this->loadCategory();
        $this->posts = $this->page['posts'] = $this->listPosts();
        $this->setValuesExtended();

        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');

            if ($currentPage > ($lastPage = $this->posts->lastPage()) && $currentPage > 1){
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
            }
        }

        $this->profile = $this->loadProfile();
    }

    protected function loadProfile() {
        $user = User::findOrFail($this->user_id);
        return $user->profile;
    }

    protected function listPosts()
    {
        $category = $this->category ? $this->category->id : null;

        /*
         * List all the posts, eager load their categories
         */
        $isPublished = !$this->checkEditor();

        $posts = BlogPost::where("user_id", $this->user_id)->with('categories')->listFrontEnd([
            'page'       => $this->property('pageNumber'),
            'sort'       => $this->property('sortOrder'),
            'perPage'    => $this->property('postsPerPage'),
            'search'     => trim(input('search')),
            'category'   => $category,
            'published'  => $isPublished,
            'exceptPost' => $this->property('exceptPost'),
        ]);

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $posts->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);

            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        });

        return $posts;
    }
}
