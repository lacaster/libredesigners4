<?php namespace Lacaster\Profile;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;
use Backend\Models\User as UserModel;
use Backend\Controllers\Users as UserController;
use Lacaster\Profile\Models\Profile as ProfileModel;
/**
 * profile Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.profile::lang.plugin.name',
            'description' => 'lacaster.profile::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {
        UserModel::extend(function($model){
            $model->hasOne['profile'] = ['Lacaster\Profile\Models\Profile'];
        });

        UserController::extendFormFields(function($form, $model, $context){



            if (! $model instanceof UserModel) {
                return;
            }

            if (! $model->exists) {
                return;
            }

            // Assegura que um model profile sempre existe
            ProfileModel::getFromUser($model);

            $form->addTabFields([
                'profile[about_me_pt_br]' => [
                    'label' => 'Sobre mim',
                    'tab' => 'Profile',
                    'type' => 'textarea',
                ],
                'profile[about_me_en]' => [
                    'label' => 'About me',
                    'tab' => 'Profile',
                    'type' => 'textarea',
                ],
                'profile[website]' => [
                    'label' => 'Web Site',
                    'tab' => 'Profile',
                ],
                'profile[diaspora]' => [
                    'label' => 'diaspora*',
                    'tab' => 'Profile',
                ],
                'profile[quitter]' => [
                    'label' => 'Quitter',
                    'tab' => 'Profile',
                ],
                'profile[software_livre_brasil]' => [
                    'label' => 'Software Livre Brasil',
                    'tab' => 'Profile',
                ],
                'profile[software_livre_brasil]' => [
                    'label' => 'Software Livre Brasil',
                    'tab' => 'Profile',
                ],
                'profile[mastodon]' => [
                    'label' => 'Mastodon',
                    'tab' => 'Profile',
                ],
                'profile[pixelfed]' => [
                    'label' => 'Pixelfed',
                    'tab' => 'Profile',
                ],
                'profile[pleroma]' => [
                    'label' => 'Pleroma',
                    'tab' => 'Profile',
                ],
                'profile[friendica]' => [
                    'label' => 'Friendica',
                    'tab' => 'Profile',
                ],
                'profile[socialhome]' => [
                    'label' => 'SocialHome',
                    'tab' => 'Profile',
                ],
                'profile[hubzilla]' => [
                    'label' => 'Hubzilla',
                    'tab' => 'Profile',
                ],
                'profile[pumpio]' => [
                    'label' => 'Pump.io',
                    'tab' => 'Profile',
                ],
                'profile[gnusocial]' => [
                    'label' => 'GNU Social',
                    'tab' => 'Profile',
                ],
                'profile[funkwhale]' => [
                    'label' => 'Funk Whale',
                    'tab' => 'Profile',
                ],
            ]);
        });
    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\Profile\Components\Profile' => 'Profile',
            'Lacaster\Profile\Components\PostsProfile' => 'PostsProfile',
            'Lacaster\Profile\Components\ProfilePost' => 'ProfilePost',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.profile.some_permission' => [
                'tab' => 'lacaster.profile::lang.plugin.name',
                'label' => 'lacaster.profile::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'profile' => [
                'label'       => 'lacaster.profile::lang.plugin.name',
                'url'         => Backend::url('lacaster/profile/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.profile.*'],
                'order'       => 500,
            ],
        ];
    }
}
