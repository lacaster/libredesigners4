<?php namespace Pitagoric\Profile\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('lacaster_profile', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('website')->nullable();
            $table->string('diaspora')->nullable();
            $table->string('quitter')->nullable();
            $table->string('software_livre_brasil')->nullable();
            $table->string('mastodon')->nullable();
            $table->string('pixelfed')->nullable();
            $table->string('pleroma')->nullable();
            $table->string('friendica')->nullable();
            $table->string('socialhome')->nullable();
            $table->string('hubzilla')->nullable();
            $table->string('pumpio')->nullable();
            $table->string('gnusocial')->nullable();
            $table->string('funkwhale')->nullable();
            $table->text('about_me_pt_br')->nullable();
            $table->text('about_me_en')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('lacaster_profile');
    }
}
