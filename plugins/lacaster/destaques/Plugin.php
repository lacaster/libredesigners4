<?php namespace Lacaster\Destaques;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * destaques Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.destaques::lang.plugin.name',
            'description' => 'lacaster.destaques::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\Destaques\Components\Destaques' => 'destaques',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return [
            'lacaster.destaques.some_permission' => [
                'tab' => 'lacaster.destaques::lang.plugin.name',
                'label' => 'lacaster.destaques::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }
}
