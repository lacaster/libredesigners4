<?php namespace Lacaster\Destaques\Models;

use Model;

/**
 * destaques Model
 */
class Destaques extends Model
{
    use \Winter\Storm\Database\Traits\Validation;
    use \Winter\Storm\Database\Traits\Sortable;

    public $implement = [
        'Winter.Translate.Behaviors.TranslatableModel'
    ];


    public $translatable = [
        'titulo',
        'descricao',
        'link'
    ];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'lacaster_destaques';

    public $timestamps = false;

    /**
     * @var array Validation rules for attributes
     */
    public $rules = ["imagem" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=600,height=400"];
    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'imagem' => 'System\Models\File'
    ];
}
