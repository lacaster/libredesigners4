<?php namespace Lacaster\Destaques\Components;

use Cms\Classes\ComponentBase;
use Lacaster\Destaques\Models\Destaques as DestaquesModel;

class Destaques extends ComponentBase
{
    public $destaques;

    public function componentDetails()
    {
        return [
            'name'        => 'Destaques',
            'description' => '3 Destaques'
        ];
    }

    protected function loadDestaques() {
        $destaques = DestaquesModel::take(3)->get();
        if (sizeof($destaques) != 3) {
            return;
        } else {
            return $destaques;
        }
    }

    public function onRun()
    {
        $this->destaques = $this->loadDestaques();
        $this->addCss('/plugins/lacaster/destaques/assets/css/destaques.css');
    }
}
