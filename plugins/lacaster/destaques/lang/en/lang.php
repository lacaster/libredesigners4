<?php

return [
    'plugin' => [
        'name' => 'destaques',
        'description' => 'No description provided yet...',
    ],
    'permissions' => [
        'some_permission' => 'Some permission',
    ],
    'models' => [
        'destaques' => [
            'label' => 'Destaque',
            'label_plural' => 'Destaques',
        ],
        'general' => [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],
    ],
];
