<?php namespace Lacaster\CatClub\Components;

use Cms\Classes\ComponentBase;

class CatGenerator extends ComponentBase
{
    public $cat_url;

    public function componentDetails()
    {
        return [
            'name'        => 'Cat Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'user_id_name' => [
                'title'       => 'User id + name',
                'description' => 'Seed the generator of cats.',
                'default'     => '{{ :uidn }}',
                'type'        => 'string',
            ],
        ];
    }

    public function onRun()
    {
        $this->render_cat();
    }

    public function build_cat(){

        $seed = $this->property('user_id_name');

        // init random seed
        if($seed) srand( hexdec(substr(md5($seed),0,6)) );

        // throw the dice for body parts
        $parts = array(
            'body' => rand(1,15),
            'fur' => rand(1,10),
            'eyes' => rand(1,15),
            'mouth' => rand(1,10),
            'accessorie' => rand(1,20)
        );

        // create backgound
        $cat = @imagecreatetruecolor(70, 70)
            or die("GD image create failed");
        $white = imagecolorallocate($cat, 255, 255, 255);
        imagefill($cat,0,0,$white);

        // add parts
        foreach($parts as $part => $num){
            $file = dirname(__FILE__).'/../assets/images/avatars/'.$part.'_'.$num.'.png';

            $im = @imagecreatefrompng($file);
            if(!$im) die('Failed to load '.$file);
            imageSaveAlpha($im, true);
            imagecopy($cat,$im,0,0,0,0,70,70);
            imagedestroy($im);
        }

        // restore random seed
        if($seed) srand();

        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header('Content-Type: image/jpg');
        imagejpeg($cat, NULL, 90);
        imagedestroy($cat);
    }

    public function render_cat(){


        $seed = $this->property('user_id_name');


        $imageurl = $seed;
        $imageurl = preg_replace('/[^A-Za-z0-9\._-]/', '', $imageurl);
        $imageurl = substr($imageurl,0,35).'';
        $cachefile = dirname(__FILE__).'/../assets/images/cache/'.''.$imageurl.'.jpg';
        $cachetime = 604800; # 1 week (1 day = 86400)

        // Serve from the cache if it is younger than $cachetime
        if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
            header('Pragma: public');
            header('Cache-Control: max-age=86400');
            header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
            header('Content-Type: image/jpg');
            readfile($cachefile);
            exit;
        }


        // ...Or start generation
        ob_start();

        // render the picture:
        $this->build_cat($seed);

        // Save/cache the output to a file
        $savedfile = fopen($cachefile, 'w+'); # w+ to be at start of the file, write mode, and attempt to create if not existing.
        fwrite($savedfile, ob_get_contents());
        fclose($savedfile);

        ob_end_flush();
    }
}
