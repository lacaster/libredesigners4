var Comment = function () {
    var self = {
        parent_id: null,
        commentName: '#comment-form',
        cancelName: '#cancel-comment-reply-link',
        commentWrapName: '#wrap-comment-form',
        messageName: '#comment_flash_message',
    };
    return {

        reply: function (event, id) {
            event.preventDefault();
            this.clean();

            self.parent_id = id;
            this.clearMessage();
            $('#comment-' + self.parent_id).find('.comment-content').append($(self.commentName));
            $(self.cancelName).show();
        },

        edit: function (event, id, content, editString, cancelString) {

            event.preventDefault();
            this.clean();


            $(".editContent").remove();
            $("#comment-" + id + " .comment-header + div > p").hide();
            $("#comment-" + id + " .comment-footer").hide();
            this.getEditForm(id, content, editString);
            this.cancelEditLink(id, cancelString);

        },

        clean: function () {
            this.cancel();
            this.clearMessage();
            $(".comment-header + div > p").show();
            $(".cancel-link").remove();
            $(".comment-footer").show();
            $(".editContent").remove();
        },

        cancelEditLink: function(id, cancelString) {
            console.log("cancelEditLink");
            $div = $("<div>");
            $div.addClass("cancel-link");
            $a = $("<a onclick='Comment.clean()'>").addClass("comment-reply");
            $a.text(cancelString);
            $div.append($a);
            $("#comment-" + id + " .comment-content").append($div);

        },

        getEditForm: function(id, content, editString) {

            $editForm = $("<form>");

            $divEdit = $("<div>").addClass("form-group").addClass("editContent");
            $textArea = $('<textarea name="content" cols="50" rows="10">').addClass("form-control").text(content);

            $divEdit.append($textArea);

            $divBtEdit = $("<div>").addClass("form-group").addClass("editContent");
            $button = $('<button type="submit" onClick="Comment.editButton(event,' + id + ')">').addClass("btn btn-primary").text(editString);

            $divBtEdit.append($button);

            $editForm.append($divEdit, $divBtEdit);

            $("#comment-" + id + " .comment-header + div").append($editForm);

        },

        updateValue: function(id, content){

            $("#comment-" + id + " .comment-header + div > p").text(content);
            this.clean();

        },

        editButton: function(event, id) {

            event.preventDefault();
            self = this;
            $("#comment-"+ id + " form").request("onEditCommentButton",{

		loading: $.oc.stripeLoadIndicator,
                data: { 'id': id },
                success: function(data) {
                    if(data["status"] === "success"){
                        $.oc.flashMsg({
                            'text': data["message"],
                            'class': 'success',
                            'interval': 4
                        });
                        location.reload();
                    }
                },
                error: function(obj, textStatus, errorObj) {

                    $.oc.flashMsg({
                        'text': obj.responseJSON.error,
                        'class': 'error',
                        'interval': 4
                    });
                }

            });

        },

        saveButton: function (event) {
            event.preventDefault();



            $(self.commentName + ' form').request('onSaveCommentButton', {
                data: {'parent_id': self.parent_id},
		loading: $.oc.stripeLoadIndicator,
/*                success: function (data) {*/
                    //if (data['message']) {
                        //Comment.addMessage(data['message'])
                    //} else if (data['content']) {
                        //Comment.addComment(data['content']);
                        //Comment.cancel();
                        //$(self.commentName + ' form').trigger('reset');
                    //} else{
                        //$(self.commentName + ' form').trigger('reset');
                    //}
                /*},*/
                success: function(data) {
                    $.oc.flashMsg({
                        'text': data["message"],
                        'class': 'success',
                        'interval': 4
                    });
                    Comment.addComment(data['content']);
                    Comment.cancel();
                    $(self.commentName + ' form').trigger('reset');
                },
                error: function(obj, textStatus, errorObj) {
                    $.oc.flashMsg({
                        'text': obj.responseJSON.error,
                        'class': 'error',
                        'interval': 4
                    });
                }
            });
        },

        addComment: function (content) {
            var commentBlock = $('#comment-' + self.parent_id);
            if (self.parent_id == null) {
                $('.comments ul:eq(0)').append(content);
            } else if (commentBlock.next('ul').length) {
                commentBlock.next('ul').append(content);
            } else {
                commentBlock.append($('<ul>').html(content));
            }
            this.countIncrement()
        },

        countIncrement: function () {
            var data = $('#comments-count').text();
            if ($.isNumeric(data)) {
                $('#comments-count').text(parseInt(data) + 1);
            }

        },

        addMessage: function (data) {
            var html = $('<ul>');
            $.each(data, function (i, item) {
                html.append($('<li>').text(item[0]));
            });
            $(self.messageName).html($("<div>").addClass('alert alert-danger').append(html));
        },

        clearMessage: function () {
            $(self.messageName).empty();
        },

        cancel: function () {
            self.parent_id = null;
            this.clearMessage();
            $(self.cancelName).hide();
            $(self.commentWrapName).html($(self.commentName));
            $("#comment-text").val('');
        },

        remove: function(event, id) {
            event.preventDefault();
            this.clean();
            $.request('onRemoveCommentButton', {
                data: {'id': id},
		loading: $.oc.stripeLoadIndicator,
                success: function(data) {
                    $.oc.flashMsg({
                        'text': data["message"],
                        'class': 'success',
                        'interval': 4
                    });
                    $('#comment-' + id).closest('li').remove();
                },
                error: function(obj, textStatus, errorObj) {

                    $.oc.flashMsg({
                        'text': obj.responseJSON.error,
                        'class': 'error',
                        'interval': 4
                    });
                }
            });
        }
    }
}();

