<?php namespace Lacaster\CatClub\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('lacaster_catclub_comments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable();
            $table->integer('post_id')->nullable();
            $table->string('author', 50)->nullable();
            $table->text('content');
            $table->smallInteger('status');
            $table->string('email', 100)->nullable();
            $table->foreign('parent_id')->references('id')->on('lacaster_catclub_comments')->onUpdate('cascade')->onDelete('cascade');
            $table->string('url', 70)->index();
            $table->string('content', 500)->nullable(false)->unsigned(false)->default(null)->change();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('lacaster_catclub_comments');
    }
}
