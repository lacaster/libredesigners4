<?php namespace Lacaster\CatClub\Models;

use Model;
use Winter\User\Models\User as User;

/**
 * Comments Model
 */
class Comments extends Model
{
    use \Winter\Storm\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lacaster_catclub_comments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
            'author' => 'min:2|max:25',
            'email' => 'email',
            'content' => 'required|min:2|max:500'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
       'user' => ['Winter\User\Models\User']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getAvatarAttribute()
    {
        $user = User::find($this->user_id);

        if($user->avatar){
            return "<img src='" .  $user->avatar->path . "' alt=''/>";
        } else {
            return "<img src='" .  url('/cat-generator') . "/" . $this->user_id.$this->author . "' alt=''/>";
        }
    }
}
