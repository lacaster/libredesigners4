<?php

return [
    'plugin' => [
        'name' => 'Comments',
        'description' => 'Allows users comment content',
    ],
    'comment' => [
        'author' => 'Autor',
        'content' => 'Conteúdo',
        'status' => 'Status',
        'comments' => 'Comentários',
    ],
    'comments' => [
        'comments' => 'comentários',
        'seconds' => 'segundos',
        'minutes' => 'minutos',
        'hours' => 'horas',
        'days' => 'dias',
        'ago' => 'atrás',
        'remove' => 'remover',
        'reply' => 'responder',
        'add_comment' => 'Comentários',
        'leave_comment' => 'Deixe um comentário',
        'send' => 'Enviar',
        'cancel_reply' => 'Cancelar resposta',
        'edit' => 'editar',
        'cancel' => 'cancelar',
        'error_comment_user' => "Comentário não pertence ao usuário.",
        'error_no_auth' => "Usuário não autenticado.",
        'deleted_sucessfully' => "Comentário removido com sucesso.",
        'success' => "Comentário realizado com sucesso.",
	'edited_sucessfully' => "Comentário editado com sucesso.",

    ],

];
