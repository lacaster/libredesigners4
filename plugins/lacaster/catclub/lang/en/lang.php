<?php

return [
    'plugin' => [
        'name' => 'Comments',
        'description' => 'Allows users comment content',
    ],
    'comment' => [
        'author' => 'Author',
        'content' => 'Content',
        'status' => 'Status',
        'comments' => 'Comments',
    ],
    'comments' => [
        'comments' => 'comments',
        'seconds' => 'seconds',
        'minutes' => 'minutes',
        'hours' => 'hours',
        'days' => 'days',
        'ago' => 'ago',
        'remove' => 'remove',
        'reply' => 'reply',
        'add_comment' => 'Add comment',
        'leave_comment' => 'Leave a comment',
        'send' => 'Send',
        'cancel_reply' => 'Cancel reply',
        'edit' => 'edit',
        'cancel' => 'Cancel',
        'error_comment_user' => "This comment doesn't belongs to you.",
        'error_no_auth' => "User is not authenticated.",
        'deleted_sucessfully' => "Comment deleted sucessfully.",
        'success' => "Comment successfully posted.",
        'edited_sucessfully' => "Comment successfully posted.",
    ],
    'models' => [
        'comment' => [
            'label' => 'Comment',
            'label_plural' => 'Comments',
        ],
    ],
];
