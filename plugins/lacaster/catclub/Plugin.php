<?php namespace Lacaster\CatClub;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * catClub Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.catclub::lang.plugin.name',
            'description' => 'lacaster.catclub::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\CatClub\Components\Comments' => 'comments',
            'Lacaster\CatClub\Components\CatGenerator' => 'catGenerator'
        ];
    }

    public function registerPermissions()
    {
        return [
            'pitagoric.catclub.some_permission' => [
                'tab' => 'catclub',
                'label' => 'Some permission'
            ],
        ];
    }

    public function registerSettings() {

        return [
        ];
    }
}
