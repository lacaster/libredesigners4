<?php namespace Lacaster\LikeBlogPost;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * likeBlogPost Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.likeblogpost::lang.plugin.name',
            'description' => 'lacaster.likeblogpost::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\LikeBlogPost\Components\LikeButtonComponent' => 'LikeButton'
        ];

    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.likeblogpost.some_permission' => [
                'tab' => 'lacaster.likeblogpost::lang.plugin.name',
                'label' => 'lacaster.likeblogpost::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'likeblogpost' => [
                'label'       => 'lacaster.likeblogpost::lang.plugin.name',
                'url'         => Backend::url('lacaster/likeblogpost/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.likeblogpost.*'],
                'order'       => 500,
            ],
        ];
    }
}
