$(document).ready(function() {
    $("#form-like-button .like-button").click(function() {
        $(this).toggleClass("ativo");
        const qtdLikesContainer = $(".likes .value");
        const spanTexto = $("#form-like-button span");
        let qtdLikesCount = parseInt(qtdLikesContainer.text());


        let message = "";

        if($(this).hasClass("ativo")) {
            qtdLikesContainer.text(++qtdLikesCount);
            message = "Você curtiu a postagem! :-) ";
            spanTexto.text("Curtiu");
        } else {
            qtdLikesContainer.text(--qtdLikesCount);
            message = "Você descurtiu a postagem... :-(";
            spanTexto.text("Curtir");
        }

        $.wn.flashMsg({
            'text': message,
            'class': 'success',
            'interval': 5
        });
    });
});
