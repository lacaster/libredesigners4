<?php namespace Lacaster\LikeBlogPost\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLacasterLikebutton extends Migration
{
    public function up()
    {
        Schema::create('lacaster_likebutton', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('blogpost_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('lacaster_likebutton');
    }
}
