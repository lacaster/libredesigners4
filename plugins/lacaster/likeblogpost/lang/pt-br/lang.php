<?php return [
    'plugin' => [
        'name' => 'LikeButton',
        'description' => 'Like button for blog posts.'
    ],
    'like' => [
        'like' => 'Curtir',
        'liked_one' => 'curtida',
        'liked_many' => 'curtidas'
    ]
];
