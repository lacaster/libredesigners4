<?php

return [
    'plugin' => [
        'name' => 'LikeButton',
        'description' => 'Like button for blog posts.',
    ],
    'like' => [
        'like' => 'Like',
        'liked_one' => 'like',
        'liked_many' => 'likes',
    ],
    'models' => [
        'general' => [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],
    ],
];
