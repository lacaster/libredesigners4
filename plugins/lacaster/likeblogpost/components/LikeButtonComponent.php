<?php namespace Lacaster\LikeBlogPost\Components;

use Cms\Classes\ComponentBase;
use Lacaster\LikeBlogPost\Models\LikeButton;
use Winter\Blog\Models\Post as BlogPost;
use System\Classes\CombineAssets;
use BackendAuth;
use Auth;
use ApplicationException;

class LikeButtonComponent extends ComponentBase
{
    public $liked;
    public $total_liked;
    public $post_id;
    public $user_id;
    public $user;

    public function componentDetails()
    {
        return [
            'name'        => 'LikeButton Component',
            'description' => 'Um botão de like para blogposts...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'rainlab.blog::lang.settings.post_slug',
                'description' => 'rainlab.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
        ];
    }

    private function setEstadoItens() {
        $user = Auth::getUser();
        $this->user = Auth::getUser();
        $post = $this->loadPost();

        $this->post_id = $post->id;

        if(!empty($user)) {
            $likedButton = LikeButton::where([
                ["user_id","=",$user->id],
                ["blogpost_id","=",$post->id]
            ])->first();
            $this->user_id = $user->id;
        } else {
            $likedButton = null;
        }

        if( !empty($likedButton) ){
            $this->liked = 1;
        } else {
            $this->liked = 0;
        }

        $this->total_liked = LikeButton::where("blogpost_id",$post->id)->count();
    }


    protected function loadPost()
    {
        $slug = $this->property('slug');

        $post = new BlogPost;

        $post = $post->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $post->transWhere('slug', $slug)
            : $post->where('slug', $slug);

        if (!$this->checkEditor()) {
            $post = $post->isPublished();
        }

        $post = $post->first();

        return $post;
    }

    public function onRun(){
        $this->setEstadoItens();


        $assetsCss = [
            'assets/css/likebutton.css'
        ];

        $assetsJs = [
            'assets/js/likebutton.js'
        ];

        $this->addCss(CombineAssets::combine($assetsCss, base_path('plugins/lacaster/likeblogpost')));
        $this->addJs(CombineAssets::combine($assetsJs, base_path('plugins/lacaster/likeblogpost')));


    }

    public function onClickLikeButton() {

         $userId = Auth::getUser()->id;
         $postId = $this->loadPost()->id;

         $like = LikeButton::where("user_id", $userId)->where("blogpost_id", $postId)->get();

         if ($like->isEmpty()) {
             LikeButton::insert([
                 "user_id" => $userId,
                 "blogpost_id" => $postId
             ]);
         } else {
             LikeButton::where("user_id",$userId)
                 ->where("blogpost_id",$postId)
                 ->delete();
         }


    }

    protected function checkEditor()
    {
        $backendUser = BackendAuth::getUser();
        return $backendUser && $backendUser->hasAccess('rainlab.blog.access_posts');
    }
}
