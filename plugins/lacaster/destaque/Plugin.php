<?php namespace Lacaster\Destaque;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * destaque Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.destaque::lang.plugin.name',
            'description' => 'lacaster.destaque::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {

        return [
            'Lacaster\Destaque\Components\Destaque' => 'destaque',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return [
            'lacaster.destaque.some_permission' => [
                'tab' => 'lacaster.destaque::lang.plugin.name',
                'label' => 'lacaster.destaque::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
}
