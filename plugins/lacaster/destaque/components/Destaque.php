<?php namespace Lacaster\Destaque\Components;

use Cms\Classes\ComponentBase;
use Lacaster\Destaque\Models\Destaque as DestaqueModel;

class Destaque extends ComponentBase {

    public $destaque;

    public function componentDetails() {
        return [
            'name' => 'Destaque',
            'description' => 'Um único destaque'
        ];
    }

    protected function loadDestaque() {
        return DestaqueModel::first();
    }

    public function onRun(){

        $this->addCss('/plugins/lacaster/destaque/assets/css/destaque.css');
        $this->destaque = $this->loadDestaque();
    }

}
