<?php namespace Lacaster\Destaque\Models;

use Model;

/**
 * destaque Model
 */
class Destaque extends Model
{
    use \Winter\Storm\Database\Traits\Validation;
    use \Winter\Storm\Database\Traits\Sortable;

    public $implement = [
        'Winter.Translate.Behaviors.TranslatableModel'
    ];


    public $translatable = [
        'titulo',
        'descricao',
        'link'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lacaster_destaque';


    public $rules = ["imagem" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1920,height=500"];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];


    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachMany = [];

    public $attachOne = [
        'imagem' => 'System\Models\File'
    ];
}
