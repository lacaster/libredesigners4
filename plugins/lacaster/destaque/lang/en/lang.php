<?php

return [
    'plugin' => [
        'name' => 'destaque',
        'description' => 'No description provided yet...',
    ],
    'permissions' => [
        'some_permission' => 'Some permission',
    ],
    'models' => [
        'general' => [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],
        'destaque' => [
            'label' => 'Destaque',
            'label_plural' => 'Destaques',
        ],
    ],
];
