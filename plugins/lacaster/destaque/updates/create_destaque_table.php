<?php namespace Lacaster\Destaque\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDestaqueTable extends Migration
{
    public function up()
    {
        Schema::create('lacaster_destaque', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo')->nullable();
            $table->text('descricao')->nullable();
            $table->text('link')->nullable();
            $table->integer('sort_order')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();

        });
    }

    public function down()
    {
        Schema::dropIfExists('lacaster_destaque');
    }
}
