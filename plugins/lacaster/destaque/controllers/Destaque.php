<?php namespace Lacaster\Destaque\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Destaque Backend Controller
 */
class Destaque extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class,
        \Backend\Behaviors\ReorderController::class,
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Lacaster.Destaque', 'main-menu-item');
    }
}
