<?php namespace Lacaster\PostsExtended\Components;

use Input;
use Redirect;
use Cms\Classes\ComponentBase;
use Winter\Blog\Models\Post;
use Cms\Classes\Page;
use PKleindienst\BlogSearch\Components\SearchResult;
use Lacaster\Catclub\Models\Comments as Comments;
use Lacaster\LikeBlogPost\Models\LikeButton;

use Winter\Blog\Models\Category as BlogCategory;
use Winter\Blog\Models\Post as BlogPost;

class SearchResultsExtended extends SearchResult
{

    public $posts;
    public $count_posts;
    public $count_likes;
    public $count_comments;

    public function componentDetails()
    {
        return [
            'name'        => 'SearchResultsExtended Component',
            'description' => 'Mostra resultados de busca com quantidade de likes e quantidade comentários...'
        ];
    }


    public function setValuesExtended()
    {
        $posts = Post::with('categories')->get();
        $count_likes = [];
        $count_comments = [];

        foreach ($posts as $p) {
            $count_likes[$p->id] = LikeButton::where("blogpost_id",$p->id)->count();
            $count_comments[$p->id] = Comments::where("post_id",$p->id)->count();
        }

        $this->count_comments = $count_comments;
        $this->count_likes = $count_likes;
    }

    public function onRun()
    {
        $this->prepareVars();

        // map get request to :search param
        $searchTerm = Input::get('search');
        if (!$this->property('disableUrlMapping') && \Request::isMethod('get') && $searchTerm) {
            // add ?cats[] query string
            $cats = Input::get('cat');
            $query = http_build_query(['cat' => $cats]);
            $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
            $query = !empty($query) ? '?' . $query : '';

            return Redirect::to(
                $this->currentPageUrl([
                    $this->searchParam => urlencode($searchTerm)
                ])
                . $query
            );
        }

        // load posts
        $posts = $this->listPosts();
        $this->posts = $this->page[ 'posts' ] = $posts;
        $this->setValuesExtended();



        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');

            if ($currentPage > ($lastPage = $this->posts->lastPage()) && $currentPage > 1) {
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
            }
        }
    }
}
