<?php namespace Lacaster\PostsExtended\Components;


use Redirect;
use BackendAuth;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Winter\Blog\Models\Post as BlogPost;
use Winter\Blog\Models\Category as BlogCategory;
use Lacaster\Catclub\Models\Comments as Comments;
use Winter\Blog\Components\Posts;
use Lacaster\LikeBlogPost\Models\LikeButton;
use Session;

// Falta fazer o esquema de paginação para as postagens
class PostsExtended extends Posts
{

    public $posts;
    public $count_posts;
    public $count_likes;
    public $count_comments;

    public function componentDetails()
    {
        return [
            'name'        => 'PostsExtended Component',
            'description' => 'Mostra postanges com quantidade de likes e quantidade comentários...'
        ];
    }

    public function setValuesExtended()
    {

        $posts = BlogPost::with('categories')->get();
        $count_likes = [];
        $count_comments = [];

        foreach ($posts as $p) {
            $count_likes[$p->id] = LikeButton::where("blogpost_id",$p->id)->count();
            $count_comments[$p->id] = Comments::where("post_id",$p->id)->count();
        }

        $this->count_comments = $count_comments;
        $this->count_likes = $count_likes;
    }



    public function onRun(){
        $this->prepareVars();

        $this->category = $this->page['category'] = $this->loadCategory();

        $this->posts = $this->listPosts();

        $this->setValuesExtended();
    }

}
