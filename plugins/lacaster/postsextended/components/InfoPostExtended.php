<?php namespace Lacaster\PostsExtended\Components;


use Redirect;
use BackendAuth;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Winter\Blog\Models\Post as BlogPost;
use Winter\Blog\Models\Category as BlogCategory;
use Lacaster\Catclub\Models\Comments as Comments;
use Winter\Blog\Components\Posts;
use Lacaster\LikeBlogPost\Models\LikeButton;

// Falta fazer o esquema de paginação para as postagens

class InfoPostExtended extends Posts
{

    public $count_likes;
    public $count_comments;
    public $count_views;

    public function componentDetails()
    {
        return [
            'name'        => 'Info PostExtended Component',
            'description' => 'Mostra informaões da postagem...'
        ];
    }


    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Slug',
                'description' => 'Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ]
        ];
    }

    public function setValuesExtended()
    {
        $slug = $this->property('slug');
        $post = BlogPost::where('slug', $slug)->first();

        $count_likes = LikeButton::where("blogpost_id",$post->id)->count();
        $count_comments = Comments::where("post_id",$post->id)->count();

        $this->count_comments = $count_comments;
        $this->count_likes = $count_likes;
        $this->count_views = $post->views;
    }

    public function onRun(){
        $this->setValuesExtended();
    }
}
