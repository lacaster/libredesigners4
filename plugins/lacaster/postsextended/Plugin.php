<?php namespace Lacaster\PostsExtended;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * postsExtended Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.postsextended::lang.plugin.name',
            'description' => 'lacaster.postsextended::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            'Lacaster\PostsExtended\Components\PostsExtended' => 'postsExtended',
            'Lacaster\PostsExtended\Components\InfoPostExtended' => 'infoPostExtended',
            'Lacaster\PostsExtended\Components\SearchResultsExtended' => 'searchResultsExtended',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.postsextended.some_permission' => [
                'tab' => 'lacaster.postsextended::lang.plugin.name',
                'label' => 'lacaster.postsextended::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'postsextended' => [
                'label'       => 'lacaster.postsextended::lang.plugin.name',
                'url'         => Backend::url('lacaster/postsextended/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.postsextended.*'],
                'order'       => 500,
            ],
        ];
    }
}
