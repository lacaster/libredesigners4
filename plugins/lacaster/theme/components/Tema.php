<?php namespace Lacaster\Theme\Components;

use Cms\Classes\ComponentBase;
use Session;

class Tema extends ComponentBase
{

    public $status_tema;

    /**
     * Gets the details for the component
     */
    public function componentDetails()
    {
        return [
            'name'        => 'tema Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * Returns the properties provided by the component
     */
    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        $assetsJS = [
            'assets/js/tema.js'
        ];

        $this->addJs($assetsJS);

        Session::put("status_tema",Session::get('status_tema', 'dark'));
    }


    public function onLight() {
        Session::put("status_tema",  "light");
        return ["status_tema" => Session::get('status_tema') ];
    }

    public function onDark() {
        Session::put("status_tema",  "dark");
        return ["status_tema" => Session::get('status_tema') ];
    }

}
