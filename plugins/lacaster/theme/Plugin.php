<?php namespace Lacaster\Theme;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;
use Event;

/**
 * theme Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'lacaster.theme::lang.plugin.name',
            'description' => 'lacaster.theme::lang.plugin.description',
            'author'      => 'lacaster',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {
    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents()
    {
        return [
            'Lacaster\Theme\Components\Tema' => 'tema',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'lacaster.theme.some_permission' => [
                'tab' => 'lacaster.theme::lang.plugin.name',
                'label' => 'lacaster.theme::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'theme' => [
                'label'       => 'lacaster.theme::lang.plugin.name',
                'url'         => Backend::url('lacaster/theme/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['lacaster.theme.*'],
                'order'       => 500,
            ],
        ];
    }
}
