var $ = jQuery;

$(function(){

    const container = document.body;
    let actualTheme = localStorage.getItem("data-theme");

    init();

    function init() {
        if (actualTheme === "light") {
            setLight();
        } else {
            setDark();
        }
    }

    function setDark() {
        localStorage.setItem("data-theme", "dark");
        $("body").removeClass("light");
        $("body").addClass("dark");
        actualTheme = "dark";
    }

    function setLight() {
        localStorage.setItem("data-theme", "light");
        $("body").removeClass("dark");
        $("body").addClass("light");
        actualTheme = "light";
    }


    $(".link-theme, .link-theme--mobile").click(function() {

        $(".theme").animate({opacity: 0}, 1000, function() {

            if (actualTheme === "light") {
                setDark();
                $.request("onDark", {
                    success: showIcon
                });
            } else {
                setLight();
                $.request("onLight", {
                    success: showIcon
                });
            }

        });

        function showIcon() {
            $(".theme").animate({opacity: 1}, 1000);
        }

    });

});
