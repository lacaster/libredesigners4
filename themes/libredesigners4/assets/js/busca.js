$(document).ready(function() {

    $(".link-busca").click(function(event) {
        event.stopPropagation();
        $("#search-input").show();
        $("#search-input input").focus();
    });

    $(window).click(function(e) {

        var container = $("#search-input");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        }
    });

    $("#search-input").hide();

});
