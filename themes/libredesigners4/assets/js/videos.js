$(document).ready(function() {
    let videoAtual = 0;
    const videoMaximo = $(".video-wrapper").length;
    const videoWrapper = $(".video-wrapper");
    const listLinks = $(".video-timeline li a");
    const btProximo = $(".video-botoes .proximo");
    const btAnterior = $(".video-botoes .anterior");
    
    events();
    
    function events() {

        listLinks.click(function(event){
            videoAtual = $(this).index(".video-timeline li a");
            atualizaVideoAtivo();
        });

        btProximo.click(addNumberVideo);
        btAnterior.click(rmNumberVideo);
        btAnterior.hide();
    }
    
    
    function atualizaVideoAtivo() {
        videoWrapper.removeClass("ativo");
        videoWrapper.eq(videoAtual).addClass("ativo");
        updateBtLinks();
    }

    function addNumberVideo() {
        if(videoAtual < videoMaximo){
            videoAtual++;
        }
        atualizaVideoAtivo();
    }

    function updateBtLinks() {
        if(videoAtual === 0){
            btAnterior.hide();
        } else{
            btAnterior.show();
        }

        if(videoAtual >= videoMaximo - 1){
            btProximo.hide();
        } else {
            btProximo.show();
        }
    }

    function rmNumberVideo() {
        if(videoAtual > 0){
            videoAtual--;
        }
        atualizaVideoAtivo();
    }
});