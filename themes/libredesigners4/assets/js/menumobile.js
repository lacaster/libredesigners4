$(document).ready(function() {

    const navMobile = $("#home-nav--mobile");
    const subMenusLinks = $("#home-nav--mobile .submenu > a");


    $(".menu-icon").click(function() {
        navMobile.toggleClass("ativo");
    });

    subMenusLinks.click(function(e) {
        e.preventDefault();
        $(this).parent().toggleClass("ativo");
    });

    $(document).on('click', function(e) {
        var container = navMobile;
        var container2 = $("#header .menu-icon");
        if (!$(e.target).closest(container).length && !$(e.target).closest(container2).length) {
            container.removeClass("ativo");
        }
    });

});
