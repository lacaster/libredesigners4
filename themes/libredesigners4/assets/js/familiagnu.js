$(document).ready(function() {

    var newGnuvalueX = 0;
    var newMontanha1valueX = 90;
    var newMontanha2valueX = 0;

    $("#familia-gnu").css('right', newGnuvalueX);
    $("#montanha-inkscape-1").css('right', newMontanha1valueX);
    $("#montanha-inkscape-2").css('right', newMontanha2valueX);


    $(document).mousemove(function(e){
        var movementStrength = 75;
        var width = movementStrength / $(window).width();

        var pageX = e.pageX - ($(window).width() / 2);
        var pageReverseX = e.pageX + ($(window).width() / 2);

        var newGnuvalueX = width * pageX + 30;
        var newMontanha1valueX = (-1)* width * pageX + 120;
        var newMontanha2valueX = (-1)* (0.5) * width * pageX + 30;

        $("#familia-gnu").css('right', newGnuvalueX);
        $("#montanha-inkscape-1").css('right', newMontanha1valueX);
        $("#montanha-inkscape-2").css('right', newMontanha2valueX);
    });
});
