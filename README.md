# [Libre Designers](https://libredesigners.org)
Libre Designers é um blog colaborativo que busca promover o uso do Software Livre no Design e a aplicação do Design no Software Livre tudo feito de aprendiz para aprendiz.

## Sobre o código Fonte

O Winter CMS separa o projeto basicamente em 2 pastas. Plugins/ e Themes/. Para criar sua instância LD basta apenas usar o conteúdo que estão nessas pastas.

## Licença

O site Libre Designers e seus plugins associados são software livre de licença MIT.

## Vulnerabilidades

Por favor, se encontrar algum bug ou vulnerabilidade no código, seja discreto e envie um e-mail para admin@libredesigners.org avisando do problema que corrigiremos o mais breve possível.

